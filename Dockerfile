FROM golang:1.12-alpine3.9 as builder

RUN apk add --no-cache git pkgconfig bash make g++ librdkafka-dev

WORKDIR /app

ENV GO111MODULE on
COPY go.mod go.sum ./
RUN go mod download

COPY . .

RUN go build -v -o kafka-sink-influxdb sink.go
RUN go build -v -o consumption consumption.go


FROM alpine:3.9

RUN apk add --no-cache librdkafka

COPY --from=builder /app/kafka-sink-influxdb /usr/bin/
COPY --from=builder /app/consumption /usr/bin/

ENTRYPOINT ["/usr/bin/kafka-sink-influxdb"]
