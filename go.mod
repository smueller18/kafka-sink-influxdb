module gitlab.com/smueller18/kafka-sink-influxdb

go 1.12

require (
	github.com/alecthomas/template v0.0.0-20160405071501-a0175ee3bccc // indirect
	github.com/alecthomas/units v0.0.0-20151022065526-2efee857e7cf // indirect
	github.com/confluentinc/confluent-kafka-go v0.11.6
	github.com/fatih/structs v1.1.0
	github.com/influxdata/influxdb1-client v0.0.0-20190402204710-8ff2fc3824fc
	github.com/jeremywohl/flatten v0.0.0-20180923035001-588fe0d4c603
	github.com/stretchr/testify v1.3.0 // indirect
	gopkg.in/alecthomas/kingpin.v2 v2.2.6
)
