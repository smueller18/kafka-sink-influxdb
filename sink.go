package main

import (
	"encoding/json"
	"fmt"
	"github.com/confluentinc/confluent-kafka-go/kafka"
	"github.com/fatih/structs"
	client "github.com/influxdata/influxdb1-client/v2"
	"github.com/jeremywohl/flatten"
	"gopkg.in/alecthomas/kingpin.v2"
	"os"
	"os/signal"
	"strconv"
	"strings"
	"syscall"
	"time"
)

type current struct {
	Base                string  `json:"base"`
	CloudsAll           int     `json:"clouds_all"`
	Cod                 int     `json:"cod"`
	CoordLat            float64 `json:"coord_lat"`
	CoordLon            float64 `json:"coord_lon"`
	Dt                  int64   `json:"dt"`
	ID                  int     `json:"id"`
	MainHumidity        int     `json:"main_humidity"`
	MainPressure        int     `json:"main_pressure"`
	MainTemp            float64 `json:"main_temp"`
	MainTempMax         float64 `json:"main_temp_max"`
	MainTempMin         float64 `json:"main_temp_min"`
	Name                string  `json:"name"`
	SysCountry          string  `json:"sys_country"`
	SysID               int     `json:"sys_id"`
	SysMessage          float64 `json:"sys_message"`
	SysSunrise          int     `json:"sys_sunrise"`
	SysSunset           int     `json:"sys_sunset"`
	SysType             int     `json:"sys_type"`
	Visibility          int     `json:"visibility"`
	Weather0Description string  `json:"weather_0_description"`
	Weather0Icon        string  `json:"weather_0_icon"`
	Weather0ID          int     `json:"weather_0_id"`
	Weather0Main        string  `json:"weather_0_main"`
	WindDeg             int     `json:"wind_deg"`
	WindSpeed           float64 `json:"wind_speed"`
}

func main() {

	conf := kafka.ConfigMap{}

	influxdbAddr := kingpin.Flag("influxdb-address", "URL of influxdb").Default("http://localhost:8086").String()
	topic := kingpin.Flag("topic", "Topic").Required().String()
	groupId := kingpin.Flag("group.id", "Group ID").Required().String()
	brokers := kingpin.Flag("bootstrap-servers", "Bootstrap broker(s)").Required().String()
	xconf := kingpin.Flag("property", "CSV separated key=value librdkafka configuration properties").Short('X').String()

	kingpin.Parse()
	conf["bootstrap.servers"] = *brokers
	conf["group.id"] = *groupId

	if len(*xconf) > 0 {
		for _, kv := range strings.Split(*xconf, ",") {
			x := strings.Split(kv, "=")
			if len(x) != 2 {
				panic("-X expects a ,-separated list of confprop=val pairs")
			}
			conf[x[0]] = x[1]
		}
	}
	fmt.Println("Config: ", conf)

	sigchan := make(chan os.Signal, 1)
	signal.Notify(sigchan, syscall.SIGINT, syscall.SIGTERM)

	c, err := kafka.NewConsumer(&conf)

	if err != nil {
		fmt.Printf("Failed to create consumer: %s\n", err)
		os.Exit(1)
	}

	fmt.Printf("Created Consumer %v\n", c)

	err = c.Subscribe(*topic, nil)

	influx, err1 := client.NewHTTPClient(client.HTTPConfig{
		Addr: *influxdbAddr,
	})
	if err1 != nil {
		fmt.Println("Error creating InfluxDB Client: ", err1.Error())
		os.Exit(1)
	}

	q := client.NewQuery("CREATE DATABASE weather", "", "")
	if response, err := influx.Query(q); err == nil && response.Error() == nil {
		fmt.Println("Assume database already exists")
	}

	bp, _ := client.NewBatchPoints(client.BatchPointsConfig{
		Database: "weather",
	})

	run := true

	for run == true {
		select {
		case sig := <-sigchan:
			fmt.Printf("Caught signal %v: terminating\n", sig)
			run = false
		default:
			ev := c.Poll(100)
			if ev == nil {
				continue
			}

			switch e := ev.(type) {
			case *kafka.Message:
				fmt.Printf("%% Recieved message with offset: %d\n", e.TopicPartition.Offset)
				flat, _ := flatten.FlattenString(string(e.Value), "", flatten.UnderscoreStyle)
				res := current{}
				err = json.Unmarshal([]byte(flat), &res)
				if err != nil {
					fmt.Println(err)
				}
				point, _ := client.NewPoint(
					"current",
					map[string]string{
						"id":   strconv.Itoa(res.ID),
						"name": res.Name,
					},
					structs.Map(res),
					time.Unix(res.Dt, 0),
				)
				bp.AddPoint(point)
				err = influx.Write(bp)

			case kafka.PartitionEOF:
				fmt.Printf("%% Reached %v\n", e)
			case kafka.Error:
				fmt.Printf("%% Error: %v\n", e)
				run = false
			default:
				fmt.Printf("Ignored %v\n", e)
			}
		}
	}

	fmt.Printf("Closing consumer\n")
	_ = c.Close()
}
