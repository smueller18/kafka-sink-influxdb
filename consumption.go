package main

import (
	"encoding/json"
	"fmt"
	"github.com/confluentinc/confluent-kafka-go/kafka"
	"github.com/fatih/structs"
	client "github.com/influxdata/influxdb1-client/v2"
	"gopkg.in/alecthomas/kingpin.v2"
	"os"
	"os/signal"
	"strings"
	"syscall"
	"time"
)

type Consumption struct {
	Windkraft struct {
		VonTimestamp string  `json:"vonTimestamp"`
		BisTimestamp string  `json:"bisTimestamp"`
		Prognose     float64 `json:"prognose"`
		Istwert      float64 `json:"istwert"`
		Betreiber    string  `json:"betreiber"`
		Zeit         string  `json:"zeit"`
		Datum        string  `json:"datum"`
		PrognoseInt  int     `json:"prognoseInt"`
		IstwertInt   int     `json:"istwertInt"`
		Einheit      string  `json:"einheit"`
	} `json:"windkraft"`
	Fotovoltaik struct {
		VonTimestamp string  `json:"vonTimestamp"`
		BisTimestamp string  `json:"bisTimestamp"`
		Prognose     float64 `json:"prognose"`
		Istwert      float64 `json:"istwert"`
		Betreiber    string  `json:"betreiber"`
		Zeit         string  `json:"zeit"`
		Datum        string  `json:"datum"`
		PrognoseInt  int     `json:"prognoseInt"`
		IstwertInt   int     `json:"istwertInt"`
		Einheit      string  `json:"einheit"`
	} `json:"fotovoltaik"`
}

func main() {

	conf := kafka.ConfigMap{}

	influxdbAddr := kingpin.Flag("influxdb-address", "URL of influxdb").Default("http://localhost:8086").String()
	topic := kingpin.Flag("topic", "Topic").Default("de.transnetbw.energy.generation").String()
	groupId := kingpin.Flag("group.id", "Group ID").Required().String()
	brokers := kingpin.Flag("bootstrap-servers", "Bootstrap broker(s)").Required().String()
	xconf := kingpin.Flag("property", "CSV separated key=value librdkafka configuration properties").Short('X').String()

	kingpin.Parse()
	conf["bootstrap.servers"] = *brokers
	conf["group.id"] = *groupId

	if len(*xconf) > 0 {
		for _, kv := range strings.Split(*xconf, ",") {
			x := strings.Split(kv, "=")
			if len(x) != 2 {
				panic("-X expects a ,-separated list of confprop=val pairs")
			}
			conf[x[0]] = x[1]
		}
	}
	fmt.Println("Config: ", conf)

	sigchan := make(chan os.Signal, 1)
	signal.Notify(sigchan, syscall.SIGINT, syscall.SIGTERM)

	c, err := kafka.NewConsumer(&conf)

	if err != nil {
		fmt.Printf("Failed to create consumer: %s\n", err)
		os.Exit(1)
	}

	fmt.Printf("Created Consumer %v\n", c)

	err = c.Subscribe(*topic, nil)

	influx, err1 := client.NewHTTPClient(client.HTTPConfig{
		Addr: *influxdbAddr,
	})
	if err1 != nil {
		fmt.Println("Error creating InfluxDB Client: ", err1.Error())
		os.Exit(1)
	}

	q := client.NewQuery("CREATE DATABASE eog", "", "")
	if response, err := influx.Query(q); err == nil && response.Error() == nil {
		fmt.Println("Assume database already exists")
	}

	bp, _ := client.NewBatchPoints(client.BatchPointsConfig{
		Database: "eog",
	})

	run := true

	for run == true {
		select {
		case sig := <-sigchan:
			fmt.Printf("Caught signal %v: terminating\n", sig)
			run = false
		default:
			ev := c.Poll(100)
			if ev == nil {
				continue
			}

			switch e := ev.(type) {
			case *kafka.Message:
				fmt.Printf("%% Recieved message with offset: %d\n", e.TopicPartition.Offset)
				res := Consumption{}
				err = json.Unmarshal(e.Value, &res)
				if err != nil {
					fmt.Println(err)
				}

				timestamp, err := time.Parse("2006-01-02 15:04:05.0 -0700", res.Windkraft.VonTimestamp + " +0200")
				if err != nil {
					fmt.Println(err)
					break
				}
				fmt.Println(timestamp)
				windkraft, _ := client.NewPoint(
					*topic,
					map[string]string{
						"category": "windkraft",
					},
					structs.Map(res.Windkraft),
					timestamp,
				)
				bp.AddPoint(windkraft)

				timestamp, err = time.Parse("2006-01-02 15:04:05.0 -0700", res.Fotovoltaik.VonTimestamp + " +0200")
				if err != nil {
					fmt.Println(err)
					break
				}
				fotovoltaik, _ := client.NewPoint(
					*topic,
					map[string]string{
						"category": "fotovoltaik",
					},
					structs.Map(res.Fotovoltaik),
					timestamp,
				)
				bp.AddPoint(fotovoltaik)

				err = influx.Write(bp)

			case kafka.PartitionEOF:
				fmt.Printf("%% Reached %v\n", e)
			case kafka.Error:
				fmt.Printf("%% Error: %v\n", e)
				run = false
			default:
				fmt.Printf("Ignored %v\n", e)
			}
		}
	}

	fmt.Printf("Closing consumer\n")
	_ = c.Close()
}
